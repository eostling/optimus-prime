import React from 'react';

import {  useSubscription } from "@apollo/client";

import { ALARM_SUBSCRIPTIONS } from "../Subscription";
import ActiveAlarmWithSub from "./ActiveAlarmWithSub";

const AlarmQueue = () => {
    const { data, loading }= useSubscription(ALARM_SUBSCRIPTIONS);

    return (
        <>
            {!data ? loading : <ActiveAlarmWithSub props={data} /> }
        </>
    );
};

export default AlarmQueue;
