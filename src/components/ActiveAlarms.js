import React from 'react';

import { FixedSizeList as List } from 'react-window';
import { Flex, Stack } from '@chakra-ui/react';

const ActiveAlarms = ({ activeAlarms }) => {
    const rowRenderer = ({ index, style }) => {
        const alarm = activeAlarms[index];
        return (
            <div style={style} key={index}>
                <Flex>
                    <h1>Active Alarms
                        The active alarms is: {alarm}
                    </h1>
                </Flex>
            </div>
        );
    };

    return (
            <>
                <Stack spacing={2} flexGrow={1} overflow='auto'>
                    <List
                        height={1200}
                        itemCount={3}
                        itemSize={100}
                        itemData={activeAlarms}
                        width='100%'
                    >
                        {rowRenderer}
                    </List>
                </Stack>
            </>
    );
};

export default ActiveAlarms;
