import React, { useReducer } from 'react';
import { GET_ACTIVE_ALARMS } from "../Queries";
import { useQuery } from "@apollo/client";
import { ALARM_SUBSCRIPTIONS } from "../Subscription";

class ActiveSubs extends React.Component {
    componentDidMount() {
        this.props.ParentActiveSubs();
        this.props.handler();
    }
}

function ActiveAlarmWithSub({ props }) {
    const [alarmList, dispatch] = useReducer(
        alarmReducer, {
        list: props,
    });

    const {activeSubscription, results} = useQuery(
        GET_ACTIVE_ALARMS,
        { variables: { alarmId: props.alarmId } }
    );

    function updateHandler(alarmId) {
        dispatch({ type: 'ACKNOWLEDGE', alarmId });
    }

    const alarmReducer = (state, action) => {
        switch (action.type) {
            case 'ACKNOWLEDGE': {
                const updatedAlarm = state.list.map((datum) => {
                    if (datum.id === action.id) {
                        const retVal = {
                            ...datum,
                        };

                        return retVal;
                    }
                    return datum;
                });

                return { ...state, list: updatedAlarm };
            }
            default:
                throw new Error('oops');
        }
    };
    
    return <>
        <ActiveSubs
            {...results}
            ParentActiveSubs={()=>{
                activeSubscription({
                    document: ALARM_SUBSCRIPTIONS,
                    variables: {alarmId: props.alarmId},
                    updateQuery: (prevData, { subscriptionData })=> {
                        if(!subscriptionData.data) return prevData;
                        const addedSub = subscriptionData.data.alarmSub;
                        return Object.assign({}, prevData,{
                            alarm: {
                                alarms: [addedSub, ...prevData.alarms.data]
                            }
                        });
                    }
                });
            }}
            handler={updateHandler}
        />
    </>
};

export default ActiveAlarmWithSub;
