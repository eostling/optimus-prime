import {GET_ALARMS} from "../Queries";
import {ADD_ALARM} from "../Mutations";
import { Mutation } from 'react-apollo';

const CreateAlarm = ({ onClick }) => {
    const newAlarm={};
    return (
        <Mutation
            mutation={ADD_ALARM}
            update={(cache, { data: { addAlarm } }) => {
                const { alarms } = cache.readQuery({ query: GET_ALARMS })
                cache.writeQuery({
                    query: GET_ALARMS,
                    data: {
                        alarms: {
                            data: alarms.data.concat([addAlarm]),
                            __typename: 'Alarms'
                        }
                    }
                })
            }}
        >
            {(addAlarm, { data }) => (
                <div>
                    <button
                        onClick={() => {
                            addAlarm({
                                variables: { ...newAlarm }
                            })
                        }}
                    >
                        Create a new Alarm
                    </button>
                </div>
            )}
        </Mutation>
    )
}

export default CreateAlarm;
