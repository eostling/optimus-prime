import './App.css';
import Alarms from "./Alarms";
import AlarmQueue from "./components/AlarmQueue";

function App() {
  return (
      <>
          <div className="App">
              PRIME -- GraphQl FE
              {/*<Alarms />*/}
              <AlarmQueue />
          </div>
      </>
  );
}

export default App;
