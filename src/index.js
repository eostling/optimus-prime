import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

import reportWebVitals from './reportWebVitals';
import {
    ApolloClient,
    InMemoryCache,
    ApolloProvider,
    split,
} from "@apollo/client";

import { RestLink } from "apollo-link-rest";
import { GET_ACTIVE_ALARMS } from "./Queries";

import { WebSocketLink } from "@apollo/client/link/ws";
import { getMainDefinition } from "@apollo/client/utilities";

const rest = new RestLink({
    endpoints: {
        v1: "http://localhost:7000",
    }
});

const webSocketLink = new WebSocketLink({
    uri:'ws://localhost:3000/alarms',
    options: {
        reconnect: true,
    },
});

const splitLink = split(({query})=>{
    const definition = getMainDefinition(query);

    return (
        definition.kind === 'OperationDefinition' &&
            definition.operation === 'subscription'
    );
},
    webSocketLink,
    rest);

const client = new ApolloClient({
    link: splitLink,
    cache: new InMemoryCache(),
});

client
    .query({
        query: GET_ACTIVE_ALARMS,
    })
    .then(result => console.log('Result', result));

ReactDOM.render(
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>,
  document.getElementById('root')
);

reportWebVitals();
