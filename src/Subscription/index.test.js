
import TestRenderer from 'react-test-renderer';
import { MockedProvider } from '@apollo/client/testing';

import { GET_ACTIVE_ALARMS } from "../Queries";
import { ALARM_SUBSCRIPTIONS } from "./index";

import ActiveAlarmWithSub from "../components/ActiveAlarmWithSub";

const data =[];
const mocks =[
    {
        request: {
            query: GET_ACTIVE_ALARMS,
            variables: {
                sensorName: '41006',
            },
        },
        result: {
            data: {
                alarm: { alarmId: '1', sensorName: '4106', alarmType: 'Foo' },
            },
        },
    },
];

describe('Should get all active alarms from apollo', ()=>{
    test('Should get all active Alarms',()=>{
       const setup = TestRenderer.create(
           <MockedProvider mocks={mocks} addTypename={false}>
               <ActiveAlarmWithSub props={data} />
           </MockedProvider>,
       );

       const apolloTree = setup().toJSON();
       expect(apolloTree).toContain('');
    });

    test('should update only one alarm', ()=>{

    });
});
