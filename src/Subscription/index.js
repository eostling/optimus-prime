import gql from "graphql-tag";

/*
* How Subs work. when onAlarmSub gets connected to it listens for data
* 
* return data will be like
* {
  "data": {
    "alarmSub": {
      "sensorName": "4106 FOO BAZ BAR",
    }
  }
} */


export const ALARM_SUBSCRIPTIONS = gql`
    subscription onAlarmSub($alarmId: alarmId!){
        alarmSub(alarmId: $alarmId){
            sensorName
        }
    }
`;
