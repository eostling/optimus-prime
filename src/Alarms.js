import { useQuery } from "@apollo/client";
import { GET_ALARMS } from "./Queries";

export default function Alarms() {
    
    const { loading, error, data } = useQuery(GET_ALARMS);

    if (loading) return <p>Loading...</p>;
    if (error) return <p>Error :(</p>;

    return data.alarm.map(({ sensorName,category }) => (
        <div key={category}>
            <p>
                Sensor Name: {sensorName} ||||  Category: {category}
            </p>
        </div>
    ));
}
