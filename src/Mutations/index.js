import gql from "graphql-tag";

export const ADD_ALARM = gql`
    mutation addAlarm($input: newAlarm!){
        addAlarm(input: $newAlarm)
            @rest(type: "Alarms", path:"alarms", method: "POST", endpoint: "v1"){
                sensorName
                active
                category
                ...rest
            } 
    }
`;

export const DELETE_ALARM = gql`
    mutation deleteAlarm($input: alarmId!){
        deleteAlarm(input: $alarmId)
        @type(type: "Alarms", path:"/alarms/{args.alarmId}", method: "DELETE", endpoint: "v1"){
            alarmId    
        }
    }
`;
