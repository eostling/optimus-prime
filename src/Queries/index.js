import gql from "graphql-tag";

export const GET_ALARMS = gql`
    query alarm {
        alarm @rest(type: "Alarms", path: "/api/alarms", method: "GET", endpoint: "v1") {
            sensorName
            active
            category
        }
    }
`;

export const GET_ACTIVE_ALARMS = gql`
    query activeAlarms {
        activeAlarms @rest(type: "Alarms", path: "/api/alarms/active", method: "GET", endpoint: "v1") {
            sensorName
            alarmID
            alarmType
            priority
            category
        }
    }
`;
