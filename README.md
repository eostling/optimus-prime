Optimus-Prime POC:

***For testing purposes only in Papi***
1. Before you create the app inject 
   ````{ 
   const cors = require('cors');
   app.use(helmet());

   const corsOptions = {
   origin: 'http://localhost:3000',
   };

   app.use(cors(corsOptions));   
   }
1. Modify in Papi. Alarms.routes for getAlarms remove `passportAuthenticateJWT`
1. Repeat this step for everything endpoints that you want to test
         


----------

Lessons learned from the SPIKE

**PROS**
1. Setting up Queries are easy and are cheap
1. Having in memory cache are great and have an easy API for them.
1. Global state management is baked in
1. Apollo` provides hooks to make development faster and easier
1. Has Pub/Sub models available
1. Documentation is fantastic.
 


**CONS**
1. learnings Graphql can have a steep learning curve
1. Would require a total rewrite of the frontend
1. The mutation model for how to setup and execute correctly is very  specific
1. Subscriptions intially are somewhat tricky to setup correctly. 
